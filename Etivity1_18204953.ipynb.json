{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Student name: Dan Laffan \n",
    "Student ID: 18204953"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Problem Description: \n",
    "-  Given a tuple containing data about students with name, age, and gender data, \n",
    "-  calculate the average age for each gender, then\n",
    "-  validate that the gender code is 'Male' or 'Female', \n",
    "-  validate that the age is numeric and between 17 and 100 inclusive, and\n",
    "-  report the results including counts of errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on feedback from the Group, I have renamed two methods to make them more readable:\n",
    "\n",
    "-  'perform_now' has been renamed to 'report_gender_age_averages'\n",
    "-  'report_results' has been renamed to 'print_gender_results'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on a review of the grading rubrick:\n",
    "\n",
    "-  added extra data validation for age data, and extra invalid data\n",
    "-  extracted the code that reports the errors into a DRY method\n",
    "-  added extra reporting for age validation errors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The average age of Female students is 35.0.\n",
      "The average age of Male students is 22.0.\n",
      "There was one student with an invalid gender code.\n",
      "There were 2 students with invalid age data.\n"
     ]
    }
   ],
   "source": [
    "def student_data():\n",
    "    return ({ 'name' : 'Alan', 'gender' : 'Male', 'age' : 21},\n",
    "            { 'name' : 'Breda', 'gender' : 'Female', 'age' : 30},\n",
    "            { 'name' : 'Charlie', 'gender' : 'Male', 'age' : 23},\n",
    "            { 'name' : 'Donna', 'gender' : 'Female', 'age' : 40},\n",
    "            { 'name' : 'Eileen', 'gender' : 'Murphy', 'age' : 20}, # simulated data input error\n",
    "            { 'name' : 'Evelyn', 'gender' : 'Female', 'age' : 0}, # invalid age\n",
    "            { 'name' : 'Frank', 'gender' : 'Male', 'age' : 'Grimes'} # invalid age\n",
    "           )\n",
    "\n",
    "def average(the_list):\n",
    "    return sum(the_list) / len(the_list)\n",
    "\n",
    "def print_gender_results(gender_dictionary, gender_error_count, age_error_count):\n",
    "    for gender in gender_dictionary:\n",
    "        print(\"The average age of \" + gender + \" students is \" + str(average(gender_dictionary[gender])) + '.')\n",
    "    print_generic_error_results(gender_error_count, \"gender code\")\n",
    "    print_generic_error_results(age_error_count, \"age\")\n",
    "    \n",
    "def print_generic_error_results(number_of_errors, error_type):\n",
    "    # expects a singular form of the error_type, such as \"gender code\"\n",
    "    if number_of_errors == 0:\n",
    "        print(\"All \" + error_type + \" data is valid.\")\n",
    "    elif number_of_errors == 1:\n",
    "        print(\"There was one student with an invalid \" + error_type + \".\")\n",
    "    else: # more than one\n",
    "        print(\"There were \" + str(number_of_errors) + \" students with invalid \" + error_type + \" data.\")\n",
    "\n",
    "def report_gender_age_averages():\n",
    "    gender_error_counter = 0\n",
    "    age_error_counter = 0\n",
    "    results_dictionary = { 'Male' : [], 'Female' : [] }\n",
    "    for person in student_data():\n",
    "        if person['gender'] == 'Male' or person['gender'] == 'Female':\n",
    "            if isinstance(person['age'],int) and 17 <= person['age'] <= 100:\n",
    "                #  17 <= number <= 100 - got the idea from Stack Overflow (note 1)\n",
    "                results_dictionary[person['gender']].append(person['age'])\n",
    "            else:\n",
    "                age_error_counter += 1\n",
    "        else:\n",
    "            gender_error_counter += 1\n",
    "    print_gender_results(results_dictionary, gender_error_counter, age_error_counter)\n",
    "            \n",
    "report_gender_age_averages()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "\"Exemplary\" rubrick:\n",
    "\n",
    "-  Challenging problem [yes - relative to being a first Python project]\n",
    "-  which requires use of at least one flow control statement, [yes]\n",
    "-  at least 2 data types [yes: tuple, and multiple dictionaries inside the tuple]\n",
    "-  and 1 container [yes: the tuple], \n",
    "-  and at least two operators [yes: /, += and +].\n",
    "\n",
    "-  Correctly working implementation [yes]\n",
    "-  with efficient use of flow control elements [yes: 2x fors and an if],\n",
    "-  data types [yes: tuple that holds dictionary of strings and integers, variables as counters]\n",
    "-  and operators [yes: += is particularly efficient]. \n",
    "-  Code provided to test the implementation with salient test parameters [yes: code contains built-in data validation for age and gender]."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note 1: https://stackoverflow.com/questions/13628791/how-do-i-check-whether-an-int-is-between-the-two-numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.5",
   "language": "python",
   "name": "python3.5"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
